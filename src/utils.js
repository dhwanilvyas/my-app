import moment from "moment";
import randomcolor from "randomcolor";
import mocker from "mocker-data-generator";

export const days = [
  {
    label: "Mon",
    value: "Monday"
  },
  {
    label: "Tue",
    value: "Tuesday"
  },
  {
    label: "Wed",
    value: "Wednesday"
  },
  {
    label: "Thu",
    value: "Thursday"
  },
  {
    label: "Fri",
    value: "Friday"
  },
  {
    label: "Sat",
    value: "Saturday"
  },
  {
    label: "Sun",
    value: "Sunday"
  }
];

export const getNameInitials = name => {
  const split = name.split(" ");
  const first = split[0] ? split[0].charAt(0) : "";
  const last = split[1] ? split[1].charAt(0) : "";

  return `${first}${last}`;
};

export const computeAge = birthday => moment().diff(moment(birthday), "years");

export const makePerson = (person, yearInput) => {
  const personBirthday = moment(person.birthday);
  const yearInputBirthday = moment([
    yearInput,
    personBirthday.month(),
    personBirthday.date()
  ]).day();

  return {
    ...days[yearInputBirthday],
    birthday: person.birthday,
    age: computeAge(person.birthday),
    initials: getNameInitials(person.name),
    color: randomcolor()
  };
};

export const getBirthdaysByDay = (birthdays, dayLabel) =>
  birthdays.filter(birthday => birthday.label === dayLabel);

export const pluralize = (
  amount,
  singularForm,
  pluralForm = `${singularForm}s`
) => {
  let word = "";

  if (+amount === 1) {
    word = singularForm;
  } else {
    word = pluralForm;
  }

  return word;
};

export const generateData = async () => {
  const user = {
    firstName: {
      faker: "name.firstName"
    },
    lastName: {
      faker: "name.lastName"
    },
    birthday: {
      faker: "date.past"
    }
  };

  try {
    const data = await mocker()
      .schema("user", user, 20)
      .build();

    return data.user.map(user => ({
      name: `${user.firstName} ${user.lastName}`,
      birthday: moment(user.birthday).format("MM/DD/YYYY")
    }));
  } catch (err) {
    console.log(err);
    return [];
  }
};
