import React from "react";
import { Button, Input, Space, Typography } from "antd";

const YearInput = ({ yearInput, setYearInput, onYearUpdate }) => {
  return (
    <Space direction="vertical">
      <Typography.Text>Year</Typography.Text>
      <Input value={yearInput} onChange={e => setYearInput(e.target.value)} />
      <Button
        type="primary"
        onClick={() => {
          if (yearInput) {
            onYearUpdate();
          }
        }}
      >
        Update
      </Button>
    </Space>
  );
};

export default YearInput;
