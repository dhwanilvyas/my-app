import React from "react";
import { Card, Empty } from "antd";
import { days, getBirthdaysByDay, pluralize } from "../utils";

const CARD_WIDTH = 150;
const CARD_HEIGHT = 150;
const CARD_HEADER_HEIGHT = 20;

const Birthdays = ({ birthdays, yearInput }) => {
  const renderBirthDays = day => {
    const birthdaysByDay = getBirthdaysByDay(birthdays, day.label);

    if (!birthdaysByDay.length) {
      return (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description="No birthdays"
        />
      );
    }

    return birthdaysByDay.map((birthday, index) => {
      return (
        <Card.Grid
          key={index}
          hoverable={false}
          style={{
            width: `${Math.ceil(CARD_WIDTH / birthdaysByDay.length - 1)}px`,
            height: `${Math.ceil(CARD_HEIGHT / birthdaysByDay.length - 1)}px`,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            textTransform: "uppercase",
            fontWeight: "bold",
            color: "white",
            backgroundColor: birthday.color
          }}
        >
          {birthday.initials}
        </Card.Grid>
      );
    });
  };

  const renderNumberOfBirthDaysLabel = day => {
    const birthdaysByDay = getBirthdaysByDay(birthdays, day.label);
    return (
      <p>
        {birthdaysByDay.length
          ? `${birthdaysByDay.length} ${pluralize(
              birthdaysByDay.length,
              "birthday"
            )}`
          : ""}
      </p>
    );
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-evenly"
      }}
    >
      {days.map(day => (
        <div key={day.label}>
          <Card
            size="small"
            title={day.label}
            style={{
              width: CARD_WIDTH,
              height: CARD_HEIGHT,
              marginRight: "0.5rem",
              marginBottom: "0.5rem",
              overflowY: "scroll"
            }}
            headStyle={{
              backgroundColor: "#9575cd",
              textAlign: "right",
              textTransform: "uppercase",
              color: "white",
              fontWeight: "lighter",
              height: CARD_HEADER_HEIGHT
            }}
          >
            {birthdays.length ? renderBirthDays(day) : null}
          </Card>
          {birthdays.length ? renderNumberOfBirthDaysLabel(day) : null}
        </div>
      ))}
    </div>
  );
};

export default Birthdays;
