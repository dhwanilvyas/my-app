import React, { useState, useEffect } from "react";
import moment from "moment";
import { Alert, Layout, Row, Col, Space, Typography } from "antd";
import JsonInput from "./JsonInput/JsonInput";
import Birthdays from "./Birthdays/Birthdays";
import YearInput from "./YearInput/YearInput";
import { makePerson, generateData } from "./utils";
import "./App.css";

const { Title } = Typography;

const { Content } = Layout;

const App = () => {
  const [error, setError] = useState(null);
  const [yearInput, setYearInput] = useState("1978");
  const [jsonInput, setJsonInput] = useState([]);
  const [birthdays, setBirthdays] = useState([]);
  const [loading, setLoading] = useState(true);

  const isDataValid = data => {
    const names = [];
    const isValid = data.every(person => {
      const valid = moment(person.birthday).isValid();
      if (!valid) {
        names.push(person.name);
      }
      return valid;
    });

    if (isValid) {
      setError(null);
    } else {
      setError({
        title: "Please enter a valid date format: MM/DD/YYYY",
        description: `${names.join(",")} have incorrect date format`
      });
    }

    return isValid;
  };

  useEffect(() => {
    const setData = async () => {
      const data = await generateData();
      setJsonInput(data);
      setLoading(false);
    };
    setData();
  }, []);

  useEffect(() => {
    if (jsonInput && isDataValid(jsonInput)) {
      onYearUpdate();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [jsonInput]);

  const onYearUpdate = () => {
    if (!jsonInput) return;
    if (!isDataValid(jsonInput)) return;

    setBirthdays(
      jsonInput
        .map(person => makePerson(person, +yearInput))
        .sort((person1, person2) => {
          if (person1.age < person2.age) {
            return -1;
          }
          if (person1.age > person2.age) {
            return 1;
          }

          return 0;
        })
    );
  };

  if (loading) {
    return <h1>Loading</h1>;
  }

  return (
    <div>
      <Layout className="app-container">
        {error ? (
          <Alert
            message={error.title}
            description={error.description}
            type="error"
          />
        ) : null}
        <Content>
          <Space direction="vertical" size="middle">
            <Title level={1}>Birthday Calender</Title>
            <Birthdays birthdays={birthdays} yearInput={yearInput} />
            <Row gutter={16}>
              <Col span={12}>
                <JsonInput jsonInput={jsonInput} setJsonInput={setJsonInput} />
              </Col>
              <Col span={12}>
                <YearInput
                  yearInput={yearInput}
                  setYearInput={setYearInput}
                  onYearUpdate={onYearUpdate}
                />
              </Col>
            </Row>
          </Space>
        </Content>
      </Layout>
    </div>
  );
};

export default App;
