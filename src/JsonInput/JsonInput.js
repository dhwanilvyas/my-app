import React from "react";
import JSONInput from "react-json-editor-ajrm";

const JsonInput = ({ jsonInput, setJsonInput }) => {
  return (
    <div>
      <JSONInput
        confirmGood={false}
        theme="light_mitsuketa_tribute"
        placeholder={jsonInput}
        width="100%"
        height="400px"
        onChange={val => {
          if (!val.error && val.json !== JSON.stringify(jsonInput)) {
            setJsonInput(val.jsObject);
          }
        }}
      />
    </div>
  );
};

export default JsonInput;
